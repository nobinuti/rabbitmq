import pika
import threading

class ConsumerThread(threading.Thread):
    #Login credentials
    HOST = '86.51.14.133'
    PORT = 5672
    USER = 'maps'
    PASSWORD = 'M@ps2018'
    
    #Fleet Queue Information
    EXCHANGE = 'fleet_data'
    QUEUE = 'fleet_data_queue'
    #QUEUE = 'fleet_data'
    ROUTING_KEY = '#'

    def __init__(self, host, *args, **kwargs):
        super(ConsumerThread, self).__init__(*args, **kwargs)

        self._host = host

    def callback_func(self, channel, method, properties, body):
        print("{} received '{}'".format(self.name, body))

    def run(self):
        credentials = pika.PlainCredentials(self.USER, self.PASSWORD)

        connection = pika.BlockingConnection(pika.ConnectionParameters(
                                                host=self.HOST,
                                                port=self.PORT,
                                                credentials=credentials)
                                             )

        channel = connection.channel()

        channel.queue_bind(queue=self.QUEUE,
                           exchange=self.EXCHANGE,
                           routing_key=self.ROUTING_KEY)

        channel.basic_consume(self.QUEUE,self.callback_func)

        channel.start_consuming()

if __name__ == "__main__":
    threads = [ConsumerThread("host1")]
    for thread in threads:
        thread.start()
