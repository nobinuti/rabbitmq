# Decorators
from functools import wraps
import logging


def timer(orig_func):
	import time
	
	@wraps(orig_func)
	def wrapper(*args, **kwargs):
		logger = logging.getLogger('rabbitmq-git.ProcessingData.timer')
		t1 = time.time()
		result = orig_func(*args, **kwargs)
		t2 = time.time() - t1
		logger.info('{} ran in: {} sec'.format(orig_func.__name__, t2))
		return result
	
	return wrapper
