import dask.dataframe as dd
from dask.delayed import delayed

import pandas as pd
import psycopg2
import cx_Oracle
from sqlalchemy import create_engine
from sqlalchemy.sql import select
from sqlalchemy.sql import text
import time

from rabbitmq.general import config
from rabbitmq.general.decorators import timer

import mysql.connector
from mysql.connector import Error
import logging


class ProcessDataOracle(object):
    def __init__(self, db_section, db_conffile=None):
        self.conf_section = db_section
        self.logger = logging.getLogger('processData.ProcessDataOracle')
        self.logger.debug('Creating an instance of ProcessDataOracle')

    @timer
    def execute_query(self, sql):
        """ This function updates table with query and list_tuples_data

        :param sql:
        :return:
        """

        # Connection to the MySQL database server
        conn = None
        try:
            # connect to the Oracle server
            # read connection parameters
            params = config.config(section=self.conf_section)

            dns_tns = cx_Oracle.makedsn(params["host"], params["port"], service_name=params["service_name"])
            conn = cx_Oracle.connect(user=params["user"], password=params["password"], dsn=dns_tns)

            cursor = conn.cursor()
            cursor.execute(sql)
            conn.commit()
            self.logger.info("All Record inserted successfully ")
        except cx_Oracle.DatabaseError as error:
            self.logger.error('{} is Unreachable. {}'.format(dns_tns, error))
        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()
                self.logger.info("MySQL connection is closed")

    @timer
    def get_data_from_db(self, db_schema, db_table, db_columns=None, limit=None):
        """ This function return dataset of type dataframe with requested table name to database

        :param db_schema:
        :param db_table:
        :param db_columns:
        :param limit:
        :return: DataFrame of table requested
        """
        # Connection to the PostgreSQL database server
        if db_columns is None:
            db_columns = []
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            dns_tns = cx_Oracle.makedsn(params["host"], params["port"], service_name=params["service_name"])
            conn = cx_Oracle.connect(user=params["user"], password=params["password"], dsn=dns_tns)
            self.logger.info('Connecting to the Oracle database...')

            # create a cursor. conn.cursor will return a cursor object, you can use this cursor to perform queries
            cursor = conn.cursor()

            # execute a statement: Send messages to PostgresSQL DataBase
            # db_columns = ','.join(args)

            if not db_columns:
                list_columns = '*'
            else:
                list_columns = ','.join(map(str, db_columns))

            # sql = "SELECT " + list_columns + " FROM " + db_schema + "." + db_table + ";"
            sql = "SELECT {} FROM {}.{}".format(list_columns, db_schema, db_table)
            if limit is not None:
                sql = "SELECT {} FROM {}.{} FETCH FIRST {} ROWS ONLY".format(list_columns, db_schema, db_table, limit)
                # sql = "SELECT " + list_columns + " FROM " + db_schema + "." + db_table + " LIMIT " + limit + ";"

            self.logger.info(sql)
            self.logger.info('Retrieving Data from Database...')

            # cursor.execute(sql)
            # df_dataset = cursor.fetchone()
            df_dataset = pd.read_sql(sql, conn)

            # close the communication with the PostgreSQL
            cursor.close()
            self.logger.info('Returning table...')
            return df_dataset
        except (Exception, cx_Oracle.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    @timer
    def get_data_from_db_alchemy(self, db_schema, db_table, pk_cols, db_columns=None, limit=None):
        """ This function return dataset of type dataframe with requested table name to database

        :param db_schema:
        :param db_table:
        :param db_columns:
        :param limit:
        :return: DataFrame of table requested
        """
        # Connection to the PostgreSQL database server
        if db_columns is None:
            db_columns = []
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            # dns_tns = cx_Oracle.makedsn(params["host"], params["port"], service_name=params["service_name"])
            # conn = cx_Oracle.connect(user=params["user"], password=params["password"], dsn=dns_tns)

            self.logger.info('Connecting to the Oracle database...')


            if not db_columns:
                list_columns = '*'
            else:
                list_columns = ','.join(map(str, db_columns))

            # sql = "SELECT " + list_columns + " FROM " + db_schema + "." + db_table + ";"
            sql = "SELECT {} FROM {}.{}".format(list_columns, db_schema, db_table)
            if limit is not None:
                sql = "SELECT {} FROM {}.{} FETCH FIRST {} ROWS ONLY".format(list_columns, db_schema, db_table, limit)
                # sql = "SELECT " + list_columns + " FROM " + db_schema + "." + db_table + " LIMIT " + limit + ";"

            engine_str = 'oracle+cx_oracle://{}:{}@{}:{}/?service_name={}'.format(params["user"], params["password"], params["host"], params["port"], params["service_name"])

            # engine = create_engine(engine_str)
            # conn = engine.connect()


            self.logger.info(sql)
            self.logger.info('Retrieving Data from Database...')
            df_dataset = dd.read_sql_table(table=db_table, uri=engine_str, index_col=pk_cols)
            # df_dataset = conn.execute(sql)



            self.logger.info('Returning table...')
            return df_dataset
        except (Exception, cx_Oracle.DatabaseError) as error:
            self.logger.error(error)
        finally:
            self.logger.info('Database connection closed.')


class ProcessDataMySQL(object):
    def __init__(self, db_section, db_conffile=None):
        self.conf_section = db_section
        self.logger = logging.getLogger('processData.ProcessDataMySQL')
        self.logger.debug('Creating an instance of ProcessDataMySQL')

    def truncate_table(self, db_table):
        """ This function updates table with query and list_tuples_data

        :param db_table:
        :return:
        """
        # Connection to the MySQL database server
        conn = None
        try:
            # connect to the MySQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            connection = mysql.connector.connect(**params)

            if connection.is_connected():
                db_info = connection.get_server_info()
                self.logger.info("Connected to MySQL Server version ", db_info)
                cursor = connection.cursor()
                sql_truncate = "TRUNCATE TABLE  {}".format(db_table)
                cursor.execute(sql_truncate)
                connection.commit()
                self.logger.info("All Record Deleted successfully ")
        except mysql.connector.Error as error:
            self.logger.error("Failed to Delete all records from database table: {}".format(error))
        finally:
            if (connection.is_connected()):
                cursor.close()
                connection.close()
                self.logger.info("MySQL connection is closed")

    def delete_table(self, db_table):
        """ This function delete table with table name as parameter

        :param db_table:
        :return:
        """
        # Connection to the MySQL database server
        conn = None
        try:
            # connect to the MySQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            connection = mysql.connector.connect(**params)

            if connection.is_connected():
                db_info = connection.get_server_info()
                self.logger.info("Connected to MySQL Server version {}".format(db_info))
                cursor = connection.cursor()
                sql_drop = "DROP TABLE IF EXISTS {}".format(db_table)
                cursor.execute(sql_drop)
                connection.commit()
                self.logger.info("table {} Deleted successfully ".format(db_table))
        except mysql.connector.Error as error:
            self.logger.error("Failed to drop table {} from database table: {}".format(db_table, error))
        finally:
            if connection.is_connected():
                cursor.close()
                connection.close()
                self.logger.info("MySQL connection is closed")

    def execute_query(self, sql):
        """
        This function updates table with query and list_tuples_data

        :param sql:
        :return:
        """

        # Connection to the MySQL database server
        conn = None
        try:
            # connect to the MySQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            connection = mysql.connector.connect(**params)

            if connection.is_connected():
                db_info = connection.get_server_info()
                self.logger.info("Connected to MySQL Server version {}".format(db_info))
                cursor = connection.cursor()
                cursor.execute(sql)
                connection.commit()
                self.logger.info("All Record inserted successfully ")
        except mysql.connector.Error as error:
            self.logger.error("Failed to Execute query: {}".format(error))
        finally:
            if (connection.is_connected()):
                cursor.close()
                connection.close()
                self.logger.info("MySQL connection is closed")


class ProcessDataPostgreSQL(object):
    def __init__(self, db_section, db_conffile=None):
        self.conf_section = db_section
        self.logger = logging.getLogger('processData.ProcessDataPostgreSQL')
        self.logger.debug('Creating an instance of PndDataToDB')

    def connect(self):
        """ Test Connect to the PostgreSQL database server """
        conn = None
        try:
            # read connection parameters
            params = config.config(section=self.conf_section)

            # connect to the PostgreSQL server
            self.logger.info('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)

            # create a cursor
            cur = conn.cursor()

            # execute a statement
            self.logger.info('PostgreSQL database version:')
            cur.execute('SELECT version()')

            # display the PostgreSQL database server version
            db_version = cur.fetchone()
            print(db_version)

            # close the communication with the PostgreSQL
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    def insert_into_db_w_json(self, db_schema, db_table, message):
        """Inserts loaded messages of type json into postgres database

        :param db_schema:
        :param db_table:
        :param message:
        :return:
        """
        # Connection to the PostgreSQL database server
        conn = None
        try:
            # connect to the PostgreSQL server
            # self.logger.debug('Connecting to the PostgreSQL database...')
            # read connection parameters
            params = config.config(section=self.conf_section)

            # Sending to .217 server
            conn = psycopg2.connect(**params)

            # create a cursor
            cur = conn.cursor()

            list_columns = "(" + ",".join(list(message.keys())) + ")"
            list_replace = "(" + ",".join(["%s" for _ in range(len(list(message.keys())))]) + ");"
            list_values = [message[x] for x in message.keys()]

            # Send messages to Postgres
            self.logger.debug('Send messages to PostgreSQL: {}'.format(self.conf_section))
            request = "INSERT INTO " + db_schema + "." + db_table + list_columns + " VALUES" + list_replace

            cur.execute(request, list_values)

            conn.commit()
            cur.close()
            return 1
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')


    @timer
    def insert_into_db_w_df(self, db_schema, db_table, df_dataset):
        """Inserts loaded dataset of type dataframe into postgres database

        :param db_schema:
        :param db_table:
        :param df_dataset:
        :return:
        """
        # Connection to the PostgreSQL database server
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            # Sending to server
            conn = psycopg2.connect(**params)

            # create a cursor
            cur = conn.cursor()

            list_columns = "(" + ",".join(list(df_dataset.columns)) + ")"
            list_replace = "(" + ",".join(["%s" for _ in range(len(list(df_dataset.columns)))]) + ");"
            request = "INSERT INTO " + db_schema + "." + db_table + list_columns + " VALUES" + list_replace

            # Send messages to Postgres
            self.logger.debug('Send messages to PostgreSQL: {}'.format(self.conf_section))

            # list_values = [tuple(x) for x in df_dataset.values]
            for x in df_dataset.values:
                list_values = tuple(x)
                try:
                    cur.execute(request, list_values)
                except (Exception, psycopg2.Error) as exception:
                    self.logger.error(exception)
                    pass
                conn.commit()

            cur.close()
            return 1
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    @timer
    def insert_into_db_w_df_keys(self, db_schema, db_table, df_dataset, list_keys, list_values_update):
        """ Inserts loaded df_dataset into postgres database with duplicate constraint verification

        :param db_schema:
        :param db_table:
        :param df_dataset:
        :param list_keys:
        :param list_values_update:
        :return:
        """
        # Connection to the PostgreSQL database server
        conn = None
        try:
            # connect to the PostgreSQL server
            # self.logger.debug('Connecting to the PostgreSQL database...')
            # read connection parameters
            params = config.config(section=self.conf_section)

            # Sending to .217 server
            conn = psycopg2.connect(**params)

            # create a cursor
            cur = conn.cursor()

            list_columns = "(" + ",".join(list(df_dataset.columns)) + ")"
            list_replace = "(" + ",".join(["%s" for _ in range(len(list(df_dataset.columns)))]) + ");"
            request = "INSERT INTO " + db_schema + "." + db_table + list_columns + " VALUES" + list_replace
            # list_values = [tuple(x) for x in df_dataset.values]

            # Send messages to Postgres
            # self.logger.debug(f'Send messages to PostgreSQL: {self.conf_section}')
            self.logger.debug('Send messages to PostgreSQL: {}'.format(self.conf_section))
            for x in df_dataset.values:
                list_values = tuple(x)
                try:
                    cur.execute(request, list_values)
                    conn.commit()
                except psycopg2.Error:
                    self.logger.exception(psycopg2.Error)

            cur.close()
            return 1
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    @timer
    def get_data_from_db(self, db_schema, db_table, db_columns=None, limit=None):
        """ This function return dataset of type dataframe with requested table name to database

        :param index_col:
        :param db_schema:
        :param db_table:
        :param db_columns:
        :param limit:
        :return: DataFrame of table requested
        """
        # Connection to the PostgreSQL database server
        if db_columns is None:
            db_columns = []
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            # Sending to .217 server
            conn = psycopg2.connect(**params)
            self.logger.info('Connecting to the PostgreSQL database...')

            # create a cursor. conn.cursor will return a cursor object, you can use this cursor to perform queries
            cursor = conn.cursor()

            # execute a statement: Send messages to PostgresSQL DataBase
            # db_columns = ','.join(args)

            if not db_columns:
                list_columns = '*'
            else:
                list_columns = ','.join(map(str, db_columns))

            sql = "SELECT " + list_columns + " FROM " + db_schema + "." + db_table + ";"
            if limit is not None:
                sql = "SELECT " + list_columns + " FROM " + db_schema + "." + db_table + " LIMIT " + limit + ";"

            self.logger.info(sql)
            self.logger.info('Send messages to PostgreSQL: {}.{}'.format(db_schema, db_table))

            df_dataset = pd.read_sql(sql, conn)
            # df_dataset = dd.read_sql_table(sql, conn, index_col)

            # close the communication with the PostgreSQL
            cursor.close()
            self.logger.info('Returning table...')
            return df_dataset
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    @timer
    def get_data_from_db_w_query(self, query):
        """ This function return table of type dataframe with full query

        :param query:
        :return: DataFrame of table requested
        """
        # Connection to the PostgreSQL database server
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            # Sending to .217 server
            conn = psycopg2.connect(**params)
            self.logger.info('Connecting to the PostgreSQL database...')

            # create a cursor. conn.cursor will return a cursor object, you can use this cursor to perform queries
            cursor = conn.cursor()

            # execute a statement: Send messages to PostgresSQL DataBase
            self.logger.info('Retrieving Data from Database...')
            df_dataset = pd.read_sql(query, conn)

            # close the communication with the PostgreSQL
            cursor.close()
            self.logger.info('Returning table...')
            return df_dataset
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    @timer
    def update_table_w_query(self, query, list_tuples_data):
        """ This function updates table with query and list_tuples_data

        :param query: UPDATE query
        :param list_tuples_data: list of tuples data in order with the query request
        :return:
        """

        # Connection to the PostgreSQL database server
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            # Sending to .217 server
            conn = psycopg2.connect(**params)
            self.logger.info('Connecting to the PostgreSQL database...')

            # create a cursor. conn.cursor will return a cursor object, you can use this cursor to perform queries
            cursor = conn.cursor()

            # # execute the INSERT statement: Send messages to PostgresSQL DataBase
            self.logger.info('Executing the INSERT statement to Database...')
            cursor.executemany(query, list_tuples_data)

            # commit the changes to the database
            conn.commit()

            # close the communication with the PostgreSQL
            cursor.close()
            return 1
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    @timer
    def execute_query(self, query):
        """ This function execute the query

        :param query: UPDATE query
        :return:
        """

        # Connection to the PostgreSQL database server
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            # Sending to .217 server
            conn = psycopg2.connect(**params)
            self.logger.info('Connecting to the PostgreSQL database...')

            # create a cursor. conn.cursor will return a cursor object, you can use this cursor to perform queries
            cursor = conn.cursor()


            cursor.execute(query)

            # commit the changes to the database
            conn.commit()
            time.sleep(1)
            # close the communication with the PostgreSQL
            cursor.close()
            return 1
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    @timer
    def update_table_w_query_condition(self, db_schema, db_table, list_cols_set, list_cols_condition,
                                       list_tuples_values):
        """ This function updates table with query and list_tuples_data

        :param list_tuples_values:
        :param list_cols_condition:
        :param list_cols_set:
        :param db_table:
        :param db_schema:
        :param list_tuples_data: list of tuples data in order with the query request
        :return: 1
        """
        # Connection to the PostgreSQL database server
        conn = None
        try:
            # read connection parameters
            params = config.config(section=self.conf_section)

            conn = psycopg2.connect(**params)
            self.logger.info('Connecting to the PostgreSQL database...')

            # create a cursor. conn.cursor will return a cursor object, you can use this cursor to perform queries
            cursor = conn.cursor()

            # execute a statement: Send messages to PostgresSQL DataBase
            # query = "UPDATE pnd set updated_at = now(), capacity = %s where codification_id = %s and capacity < %s"
            # TODO not finished yet
            l_sets = " = %s, ".join(list_cols_set)
            l_conditions = " = %s AND ".join(list_cols_condition)
            sql = "UPDATE " + db_schema + "." + db_table + " SET " + l_sets + " WHERE " + l_conditions

            self.logger.info('Executing the INSERT statement to Database. Table {}.{}'.format(db_schema, db_table))
            # execute the INSERT statement
            cursor.executemany(sql, list_tuples_values)

            # commit the changes to the database
            conn.commit()

            # close the communication with the PostgreSQL
            cursor.close()
            return 1
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    @timer
    def update_table_w_query(self, sql_query, list_tuples_data):
        """ This function updates table with query and list_tuples_data

        :param sql_query: UPDATE query
        :param list_tuples_data: list of tuples data in order with the query request
        :return:
        """

        # Connection to the PostgreSQL database server
        conn = None
        try:
            # read connection parameters
            params = config.config(section=self.conf_section)

            conn = psycopg2.connect(**params)
            self.logger.info('Connecting to the PostgreSQL database...')

            # create a cursor. conn.cursor will return a cursor object, you can use this cursor to perform queries
            cursor = conn.cursor()

            # execute a statement: Send messages to PostgresSQL DataBase
            request = sql_query
            self.logger.info('Executing the UPDATE statement to Database...')
            # execute the INSERT statement
            cursor.executemany(request, list_tuples_data)

            # commit the changes to the database
            conn.commit()

            # close the communication with the PostgreSQL
            cursor.close()
            return 1
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')


class ProcessDataPostgreSQLAlchemy(object):
    def __init__(self, db_section, db_conffile=None):
        self.conf_section = db_section
        self.logger = logging.getLogger('processData.ProcessDataPostgreSQLAlchemy')
        self.logger.debug('Creating an instance of ProcessDataPostgreSQLAlchemy')

    def connect(self):
        """ Test Connect to the PostgreSQL database server """
        conn = None
        try:
            # read connection parameters
            params = config.config(section=self.conf_section)

            # connect to the PostgreSQL server
            self.logger.info('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)

            # create a cursor
            cur = conn.cursor()

            # execute a statement
            self.logger.info('PostgreSQL database version:')
            cur.execute('SELECT version()')

            # display the PostgreSQL database server version
            db_version = cur.fetchone()
            print(db_version)

            # close the communication with the PostgreSQL
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    def create_empty_table_w_df(self, df, tbl_name):
        """ before uploading the df to the db is creating an empty table in an existing database,
        so sending an empty table with the right column names will do the trick quite well

        :param tbl_name: name of table to create
        :param df: df to get column name
        :return:
        """
        # Connection to the PostgreSQL database server
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            # engine_str = 'dialect+driver://username:password@host:port/database'
            engine_str = 'postgresql+psycopg2://{}:{}@{}:5432/{}'.format(params["user"], params["password"],
                                                                         params["host"], params["dbname"])
            engine = create_engine(engine_str)

            # conn = psycopg2.connect(**params)
            conn = engine.connect()
            self.logger.info(engine_str)

            df.columns = df.columns.str.lower()
            pd.DataFrame(columns=df.columns).to_sql(name=tbl_name,
                                                    con=engine,
                                                    if_exists='replace',
                                                    index=False)

            self.logger.info('Empty table created.')
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    def insert_into_db_w_df(self, db_schema, db_table, df_dataset):
        """Inserts loaded dataset of type dataframe into postgres database

        :param db_schema:
        :param db_table:
        :param df_dataset:
        :return:
        """
        # Connection to the PostgreSQL database server
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            # engine_str = 'dialect+driver://username:password@host:port/database'
            engine_str = 'postgresql+psycopg2://{}:{}@{}:5432/{}'.format(params["user"], params["password"],
                                                                         params["host"], params["dbname"])
            engine = create_engine(engine_str)
            self.logger.info(engine_str)

            conn = engine.connect()
            trans = conn.begin()

            df_dataset.to_dask_array()
            # for row in df_dataset.iterrows():

            list_columns = "(" + ",".join(list(df_dataset.columns)) + ")"
            # list_replace = "(" + ",".join(["{}" for _ in range(len(list(df_dataset.columns)))]) + ");"
            list_replace = "(" + ",".join(["%s" for _ in range(len(list(df_dataset.columns)))]) + ");"
            request = "INSERT INTO " + db_schema + "." + db_table + list_columns + " VALUES" + list_replace
            list_colname = ['row.' + x for x in df_dataset.columns]

            for row in df_dataset.itertuples(index=False):  # doctest: +SKIP
                print('row: ')
                print(row)
                print('request')
                print(request)
                print('request.row')
                # sql = "insert into test2del(a, b) values ({}, {})".format(row[0], row[1])
                # engine.execute(text(sql).execution_options(autocommit=True))
                engine.execute(request, row)
                # df_dataset.to_sql(db_table, con=engine, if_exists='append', chunksize=1000)


            self.logger.debug('Send messages to PostgreSQL: {}'.format(self.conf_section))

            return 1
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

    @timer
    def get_data_from_db(self, db_schema, db_table, db_columns=None, limit=None):
        """ This function return dataset of type dataframe with requested table name to database

        :param index_col:
        :param db_schema:
        :param db_table:
        :param db_columns:
        :param limit:
        :return: DataFrame of table requested
        """
        # Connection to the PostgreSQL database server
        if db_columns is None:
            db_columns = []
        conn = None
        try:
            # connect to the PostgreSQL server
            # read connection parameters
            params = config.config(section=self.conf_section)

            # engine_str = 'dialect+driver://username:password@host:port/database'
            engine_str = 'postgresql+psycopg2://{}:{}@{}:5432/{}'.format(params["user"], params["password"],
                                                                         params["host"], params["dbname"])
            engine = create_engine(engine_str)
            self.logger.info('Connecting to the PostgreSQL database...')
            # conn = psycopg2.connect(**params)
            conn = engine.connect()
            self.logger.info(engine_str)

            if not db_columns:
                list_columns = '*'
            else:
                list_columns = ','.join(map(str, db_columns))

            sql = "SELECT " + list_columns + " FROM " + db_schema + "." + db_table + ";"
            if limit is not None:
                sql = "SELECT " + list_columns + " FROM " + db_schema + "." + db_table + " LIMIT " + limit + ";"

            self.logger.info(sql)
            self.logger.info('Retrieving Data from Database...')

            trans = conn.begin()

            try:
                df = conn.execute(text(sql)).fetchall()
                trans.commit()
            except Exception as error:
                trans.rollback()
                self.logger.error(error)
                raise

            self.logger.info('Returning table...')
            return df
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        finally:
            if conn is not None:
                conn.close()
                self.logger.info('Database connection closed.')

if __name__ in ['__main__', "pydev_run_in_console"]:
    service = ProcessDataPostgreSQLAlchemy(db_section='postgresql')
    # TODO Do something here
    columns = ["c", "d"]

    df1 = pd.DataFrame({'a': ['A0', 'A1', 'A2', 'A3'],
                        'b': ['B0', 'B1', 'B2', 'B3'],
                        'c': ['C0', 'C1', 'C2', 'C3'],
                        'd': ['D0', 'D1', 'D2', 'D3']},
                       index=[0, 1, 2, 3])

    service.insert_into_db_w_df(db_schema='public',
                                db_table='test2del',
                                df_dataset=df1)

    aa = service.get_data_from_db(db_schema='public',
                                  db_table='test2del', db_columns=columns)

    print(aa)