import multiprocessing
from rabbitmq.fetchingdata.maps_pnd import PndDataToDB
from rabbitmq.fetchingdata.fleet_data import FleetDataToDB
import logging
import logging.config
from logging.handlers import TimedRotatingFileHandler

import os.path


FILEPATH = os.path.abspath(os.path.dirname(__file__))
LOGCONF_FILENAME = os.path.join(FILEPATH, '../general/logging_config.ini')

# logging.config.fileConfig(fname='file.conf', disable_existing_loggers=False)
logging.config.fileConfig(fname = LOGCONF_FILENAME, defaults = {'logfilename': FILEPATH + '/../log/RabbitMQ.log'}, disable_existing_loggers = False)

# create logger with 'rabbitmq-git'
logger = logging.getLogger('rabbitmq-git')

# Begin of main function
logger.info('Starting mawgif_rmq data processing...')

exchange_name = 'maps_data'
logger.info('Create an instance of fetchingdata.maps_pnd.PndDataToDB')
rabbit_maps = PndDataToDB()
logger.info('Calling fetchingdata.maps_pnd.multiprocessing')
p_maps = multiprocessing.Process(target=rabbit_maps.consume_message, args=('maps_pnd1', exchange_name))
# rabbit_maps.consume_message(queue_name, exchange_name)


exchange_name = 'fleet_data'
logger.info('Create an instance of fetchingdata.fleet_data.FleetDataToDB')
rabbit_fleet = FleetDataToDB()
logger.info('Calling fetchingdata.fleet_data.multiprocessing')
p_fleet = multiprocessing.Process(target=rabbit_fleet.consume_message, args=('fleet_data_queue', exchange_name))
queue_name = 'fleet_data_queue'
# rabbit_fleet.consume_message(queue_name, exchange_name)

p_maps.start()
p_fleet.start()








