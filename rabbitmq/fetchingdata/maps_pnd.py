import pika
import json
from datetime import datetime
import psycopg2
from psycopg2 import connect
from retry import retry
import pika.exceptions
import logging
import os.path

from rabbitmq.general import config
from rabbitmq.general.processingdata import ProcessDataPostgreSQL

FILEPATH = os.path.abspath(os.path.dirname(__file__))
LOG_FILENAME = os.path.join(FILEPATH, '/log/sales.log')

# create logger
module_logger = logging.getLogger('rabbitmq-git.fetchingdata')
logging.basicConfig(level = logging.INFO)


class PndDataToDB(object):
	
	def __init__(self):
		# Login credentials
		HOST = '86.51.14.133'
		PORT = 5672
		USER = 'maps'
		PASSWORD = 'M@ps2018'
		
		self.rejected = {}
		self.max_rejection = 10
		self.n_operations = {"rmq_alarm": 0, "rmq_collection": 0, "rmq_indicator": 0, "rmq_pnd": 0,
							 "rmq_sale": 0, "rmq_rejected": 0}
		
		params = config.config(section = 'rabbitmq')
		credentials = pika.PlainCredentials(params['user'], params['password'])
		self.parameters = pika.ConnectionParameters(params['host'], params['port'], '/', credentials)
		# credentials = pika.PlainCredentials(USER, PASSWORD)
		# self.parameters = pika.ConnectionParameters(HOST, PORT, '/', credentials)
		self.logger = logging.getLogger('rabbitmq-git.fetchingdata.PndDataToDB')
		self.logger.debug('Creating an instance of PndDataToDB')
	
	def callback(self, ch, method, properties, body):
		"""
		Global function that parses a df_dataset and insert it into Postgres database
		:param ch:
		:param method:
		:param properties:
		:param body:
		:return:
		"""
		
		# Decode UTF-8 bytes to Unicode and convert single quotes to double quotes to make it valid JSON
		# self.logger.debug('Decode UTF-8 bytes to Unicode and convert single quotes to double quotes to make it valid JSON')
		json_body = body.decode('utf-8').replace("'", '"')
		# Load the JSON to a Python list & dump it back out as formatted JSON
		json_body = json.loads(json_body)
		# self.logger.info(json_body)
		
		# Converting key to json
		# self.logger.debug('Converting key to json')
		key = self.convert_key_2_json(method.routing_key)
		# self.logger.debug(key)
		# Parsing the df_dataset
		parsed_message = self.message_clean_format(json_body, key['msg_type'])
		# Inserting df_dataset into database
		try:
			# self.logger.debug('Insert parsed_message into database')
			self.insert_into_database(parsed_message, 'rmq_' + key['msg_type'])
			self.channel.basic_ack(method.delivery_tag, False)
			self.n_operations['rmq_' + key['msg_type']] += 1
			if method.routing_key in self.rejected.keys():
				del self.rejected[method.routing_key]
		except:
			if method.routing_key in self.rejected.keys():
				if self.rejected[method.routing_key] > self.max_rejection:
					# self.logger.debug('Write rejected df_dataset')
					self.channel.basic_ack(method.delivery_tag, False)
					with open("rejected_messages_maps.txt", "a") as myfile:
						myfile.write(str(json_body) + "\n")
					self.n_operations["rejected"] += 1
					del self.rejected[method.routing_key]
				else:
					self.channel.basic_nack(method.delivery_tag, False, True)
			else:
				self.rejected[method.routing_key] = 1
		# Printing progress
		self.printing_progress(1000)
		self.logger.info(self.printing_progress(1000))
	
	def convert_key_2_json(self, str_RoutingKey):
		"""
		Extract the type of df_dataset from the routing_key parameter
		:param str_RoutingKey:
		:return:
		"""
		# self.logger.debug('Extract the type of df_dataset from the routing_key parameter')
		parts = str_RoutingKey.split('.')
		key = {"fullkey": str_RoutingKey,
			   "verb": parts[0],
			   "country_code": parts[1],
			   "park_code": parts[2],
			   "msg_type": parts[3]
			   }
		json_key = json.dumps(key)
		jsonlist = json.loads(json_key)  # convert list to string and then load it as json to remove slashes
		# print(jsonlist)
		return jsonlist
	
	def consume_message(self, queue_name, exchange_name):
		"""
		Starts receiving messages from Rabbit MQ
		:param queue_name: name of the queue
		:param exchange: name of the exchange
		:return: None
		"""
		while True:
			try:
				# Connect
				connection = pika.BlockingConnection(parameters = self.parameters)
				self.channel = connection.channel()
				self.channel.basic_qos(prefetch_count = 1)
				
				# Init exchange
				self.channel.exchange_declare(exchange = exchange_name,
											  exchange_type = 'topic',
											  durable = True)
				
				# Init queue
				# args = {"x-df_dataset-ttl": None} # orignal: 86400000  345600000
				# args['x-max-length'] = 10000 # orignal: 10000   40000
				# args['exclusive'] = True
				args = {"exclusive": True}
				result = self.channel.queue_declare(queue = queue_name, durable = True, arguments = args)
				queue_name = result.method.queue
				
				# Bind queue to exchange
				self.channel.queue_bind(exchange = exchange_name, queue = queue_name,
										routing_key = '#')  # original routing_key = '*.*.*.sale'
				
				# Consume messages
				self.channel.basic_consume(queue = queue_name,
										   auto_ack = False,
										   on_message_callback = self.callback)
				print('channel maps_pnd starting consuming')
				self.channel.start_consuming()
			# Don't recover if connection was closed by broker
			except pika.exceptions.ConnectionClosedByBroker:
				self.logger.error(pika.exceptions.ConnectionClosedByBroker)
				break
			# Don't recover on channel errors
			except pika.exceptions.AMQPChannelError:
				self.logger.error(pika.exceptions.AMQPChannelError)
				break
			# Recover on all other connection errors including other Heartbeat Timeout errors
			except pika.exceptions.AMQPConnectionError:
				self.logger.error(pika.exceptions.AMQPConnectionError)
				continue
	
	# Recover on all other Heartbeat Timeout errors
	# except pika.exceptions.AMQPHeartbeatTimeout:
	#	continue
	
	def insert_into_database(self, message, table_name):
		"""
		Inserts loaded messages into postgres database
		:param message: parsed df_dataset to insert (dictionary)
		:param table_name: name of the table
		:return: None
		"""
		# Connection to the PostgreSQL database server
		list_values = [message[x] for x in message.keys()]
		
		# Sending to DEV server
		self.logger.debug('Sending to DEV server')
		process = ProcessDataPostgreSQL(db_section = 'postgresql_dev')
		process.insert_into_db_w_json(db_schema = 'public', db_table = table_name, message = message)
	
		# Sending to UAT server
		self.logger.debug('Sending to UAT server')
		process = ProcessDataPostgreSQL(db_section = 'postgresql_uat')
		process.insert_into_db_w_json(db_schema = 'public', db_table = table_name, message = message)
	
	def message_clean_format(self, message_json, type):
		"""
		Parses the body of the received df_dataset depending on its type
		:param message_json: body of the df_dataset (dictionary)
		:param type: type of df_dataset
		:return: parsed df_dataset (dictionary)
		"""
		
		parsed_message = {}
		
		if type == "sale":
			for column in [x for x in message_json.keys() if x not in ['products', 'payments']]:
				parsed_message[column] = message_json[column]
			# TODO check if there is always a single value in the list products
			if 'products' in message_json.keys():
				for n in range(len(message_json['products'])):
					for sub_col in message_json['products'][n]:
						column = 'products_' + sub_col
						parsed_message[column] = message_json['products'][0][sub_col]
			if 'sale_date' in message_json.keys():
				parsed_message['sale_date'] = datetime.strptime(message_json['sale_date'], "%Y-%m-%dT%H:%M:%S+00:00")
			if 'payments' in message_json.keys():
				parsed_message['payments_details'] = json.dumps(message_json['payments'], indent = 1)
			if 'products_startdate' in parsed_message.keys():
				parsed_message['products_startdate'] = datetime.strptime(parsed_message['products_startdate'],
																		 "%Y-%m-%dT%H:%M:%S+00:00")
				parsed_message['products_enddate'] = datetime.strptime(parsed_message['products_enddate'],
																	   "%Y-%m-%dT%H:%M:%S+00:00")
		
		elif type == "alarm":
			for column in [x for x in message_json.keys() if x not in ['eventstate']]:
				parsed_message[column] = message_json[column]
			# TODO check if there is always a single value in the list products
			if 'eventstate' in message_json.keys():
				for sub_col in message_json['eventstate']:
					column = 'event_' + sub_col
					parsed_message[column] = message_json['eventstate'][sub_col]
			if 'event_DBValue' in parsed_message.keys():
				parsed_message['event_DBValue'] = str(parsed_message['event_DBValue'])
			if 'receptiondate' in parsed_message.keys():
				parsed_message['receptiondate'] = datetime.strptime(parsed_message['receptiondate'],
																	"%Y-%m-%dT%H:%M:%S+00:00")
				parsed_message['terminaldate'] = datetime.strptime(parsed_message['terminaldate'],
																   "%Y-%m-%dT%H:%M:%S+00:00")
		
		elif type == "indicator":
			for column in message_json.keys():
				parsed_message[column] = message_json[column]
			if 'LAST_COMM' in parsed_message.keys():
				parsed_message['LAST_COMM'] = datetime.strptime(parsed_message['LAST_COMM'], "%Y-%m-%dT%H:%M:%S+00:00")
			if 'LAST_COIN_COL' in parsed_message.keys():
				parsed_message['LAST_COIN_COL'] = datetime.strptime(parsed_message['LAST_COIN_COL'],
																	"%Y-%m-%dT%H:%M:%S+00:00")
			if 'LAST_NOTE_COL' in parsed_message.keys():
				parsed_message['LAST_NOTE_COL'] = datetime.strptime(parsed_message['LAST_NOTE_COL'],
																	"%Y-%m-%dT%H:%M:%S+00:00")
		
		elif type == "pnd":
			for column in [x for x in message_json.keys() if x not in ['addresses']]:
				parsed_message[column] = message_json[column]
			# TODO check if there is always a single value in the list products
			if 'addresses' in message_json.keys():
				for sub_col in message_json['addresses'][0]:
					column = 'addresses_' + sub_col
					parsed_message[column] = message_json['addresses'][0][sub_col]
				parsed_message['addresses_receptiondate'] = datetime.strptime(parsed_message['addresses_receptiondate'],
																			  "%Y-%m-%dT%H:%M:%S+00:00")
		
		elif type == "collection":
			for column in [x for x in message_json.keys() if x not in ['channels']]:
				parsed_message[column] = message_json[column]
			# TODO check if there is always a single value in the list products
			if 'channels' in message_json.keys():
				parsed_message['channels_details'] = json.dumps(message_json['channels'])
			if 'collect_date' in parsed_message.keys():
				parsed_message['collect_date'] = datetime.strptime(parsed_message['collect_date'],
																   "%Y-%m-%dT%H:%M:%S+00:00")
		
		return parsed_message
	
	def printing_progress(self, freq):
		"""
		Prints information every freq messages
		:return: None
		"""
		if sum(self.n_operations.values()) % freq == 0:
			print("{}".format(datetime.now()))
			print("maps -> {} messages treated".format(sum(self.n_operations.values())))
			print("-----------")
			for key in self.n_operations.keys():
				print("{} : {}".format(key, self.n_operations[key]))
			print("-----------")


if __name__ == '__main__':
	rabbit_maps = PndDataToDB()
	rabbit_maps.consume_message(queue_name = 'maps_pnd1', exchange_name = 'maps_data')

