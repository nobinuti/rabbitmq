import pika
import sys
import json
from datetime import datetime
from psycopg2 import connect
from retry import retry


from rabbitmq.general import config
from rabbitmq.general.processingdata import ProcessDataPostgreSQL


class FleetDataToDB(object):
	
	def __init__(self):
		
		self.rejected = {}
		self.max_rejection = 10
		self.n_operations = {"fleet": 0, "rejected": 0}
		params = config.config(section = 'rabbitmq')
		credentials = pika.PlainCredentials(params['user'], params['password'])
		self.parameters = pika.ConnectionParameters(params['host'], params['port'], '/', credentials)
	
	def callback(self, channel, method, properties, body):
		# print("{} received '{}'".format(self.name, body))
		# ignore if body is empty
		if body:
			# Decode UTF-8 bytes to Unicode and convert single quotes to double quotes to make it valid JSON
			json_body = body.decode('utf-8').replace("'", '"')
			# Load the JSON to a Python list & dump it back out as formatted JSON
			# Convert bytes literals to normal json
			json_body = json.loads(json_body)
			_confirm = True
			for item in json_body:
				parsed_message = self.format_fleet_message(item)
				try:
					self.insert_into_database(parsed_message, 'rmq_fleet')
					self.n_operations['fleet'] += 1
					if method.routing_key in self.rejected.keys():
						del self.rejected[method.routing_key]
				except:
					if method.routing_key in self.rejected.keys():
						if self.rejected[method.routing_key] > self.max_rejection:
							with open("rejected_messages_fleet.txt", "a") as myfile:
								myfile.write(str(json_body) + "\n")
							self.n_operations["rejected"] += 1
							del self.rejected[method.routing_key]
						else:
							_confirm = False
					else:
						self.rejected[method.routing_key] = 1
			if _confirm:
				self.channel.basic_ack(method.delivery_tag, False)
			else:
				self.channel.basic_nack(method.delivery_tag, False, True)
			# Printing progress
			self.printing_progress(100)
	
	def run(self):
		credentials = pika.PlainCredentials(self.USER, self.PASSWORD)
		
		connection = pika.BlockingConnection(pika.ConnectionParameters(
				host = self.HOST,
				port = self.PORT,
				credentials = credentials)
		)
		
		self.channel = connection.channel()
		
		self.channel.queue_bind(queue = self.QUEUE,
								exchange = self.EXCHANGE,
								routing_key = self.ROUTING_KEY)
		
		self.channel.basic_consume(self.QUEUE, self.callback_func)
		
		self.channel.start_consuming()
	
	
	def format_fleet_message(self, message_json):
		
		message_json['Lon'] = float(message_json['Lon'])
		message_json['Lat'] = float(message_json['Lat'])
		message_json['speed'] = int(message_json['speed'])
		message_json['mileage'] = int(message_json['mileage'])
		message_json['fuel'] = int(message_json['fuel'])
		message_json['tremTime'] = datetime.strptime(message_json['tremTime'], "%m/%d/%Y %H:%M:%S %p")
		message_json['height'] = int(message_json['height'])
		
		return message_json
	
	def insert_into_database(self, message, table_name):
		# Sending to DEV server
		process = ProcessDataPostgreSQL(db_section = 'postgresql_dev')
		process.insert_into_db_w_json(db_schema = 'public', db_table = table_name, message = message)
	
		# Sending to UAT server
		process = ProcessDataPostgreSQL(db_section = 'postgresql_uat')
		process.insert_into_db_w_json(db_schema = 'public', db_table = table_name, message = message)
	
	
	def convert_key_2_json(self, str_RoutingKey):
		parts = str_RoutingKey.split('.')
		key = {"fullkey": str_RoutingKey,
			   "verb": parts[0]
			   }
		json_key = json.dumps(key)
		jsonlist = json.loads(json_key)  # convert list to string and then load it as json to remove slashes
		return jsonlist
	
	def consume_message(self, queue_name, exchange_name, routing_key = '#'):
		
		"""
		Starts receiving messages from Rabbit MQ
		:param queue_name: name of the queue
		:param exchange_name: name of the exchange
		:return: None
		"""
	
		# Connect
		connection = pika.BlockingConnection(parameters = self.parameters)
		self.channel = connection.channel()

		
		# Bind queue to exchange
		self.channel.queue_bind(exchange = exchange_name, queue = queue_name)
		
		# Consume messages
		self.channel.basic_consume(queue = queue_name,
								   # auto_ack = False,
								   on_message_callback = self.callback)
		print('channel fleet_data starting consuming')
		self.channel.start_consuming()

	def printing_progress(self, freq):
		"""
		Prints information every freq messages
		:return: None
		"""
		if sum(self.n_operations.values()) % freq == 0:
			print("{}".format(datetime.now()))
			print("fleet -> {} messages treated".format(sum(self.n_operations.values())))
			print("-----------")
			for key in self.n_operations.keys():
				print("{} : {}".format(key, self.n_operations[key]))
			print("-----------")

if __name__ == '__main__':
	rabbit_fleet = FleetDataToDB()
	rabbit_fleet.consume_message(queue_name = 'fleet_data_queue', exchange_name = 'fleet_data', routing_key = '#')

